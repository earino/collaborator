# collaboratoR - Package Proposal

**Goal**: Make it easy for API writers to understand how their users interact with their APIs. Make it easy for users to contribute to their favorite APIs in a way that enhances their experience and provides them with value. Do this all in a way that is transparent and completely non-creepy.

## Motivation

The tidyR package contains principally two functions, `spread()` and `gather()`. The two functions are theoretically two sides of the same coin, and should have similar usage. However, it eventually became obvious that there was some issue with the design of these functions. In a [tweet](https://twitter.com/JennyBryan/status/1148889857365893120), [Jenny Bryan](https://twitter.com/JennyBryan) shared her personal experience in dealing with these two functions:

![jenny's tweet](images/jenny_tweet.png)

This resonated with me, as Jenny's insights usually do. Even though I have taught graduate Data Science courses for years that leverage these functions, I find myself having to look at the help for `spread()` every time I use it. At this point, I was struck by the irony that R package authors have a paucity of data about how their packages are used interactively.

What would it look like to provide an infrastructure for R package authors to get information from their users about what challenges they encounter using their packages. What would it look like for those very users to learn and excercise the skills that provide a foundation to modern analytical collaboration by collaborating witheir favorite package authors? 

Could this even be done in a way that wouldn't be creepy and would respect user choice and be backed by the principles of the Menlo Report? Could be implemented in a way that would make enterprise administrators feel comfortable that important data wasn't being exfiltrated? 

This document attempts to sketch out what a hypothetical collaboratoR package and supporting infrastructore may look like.

## The User Experience

What would it feel like to use the collaboratoR package? I can imagine a few different approaches.

### A Sample Session

You can imagine an interactive user experiencing collaboratoR in this fashion. Imagine that this is the RStudio console. Upon loading the `tidyverse` package, it notes that there is no `collaboratoR` default 
and provides some interactive guidance:

```
> library(tidyverse)
── Attaching packages ───────────────────────────────────────── tidyverse 1.2.1 ──
✔ ggplot2 2.2.1     ✔ purrr   0.2.4
✔ tibble  1.4.2     ✔ dplyr   0.7.4
✔ tidyr   0.8.0     ✔ stringr 1.3.1
✔ readr   1.1.1     ✔ forcats 0.3.0
── Conflicts ──────────────────────────────────────────── tidyverse_conflicts() ──
✖ dplyr::filter() masks stats::filter()
✖ dplyr::lag()    masks stats::lag()
── Provide feedback ──────────────────────────────────── collaboratoR() --
We see that you don't have a collaboratoR default set. Collaborator
is a package that allows you to communicate with the Tidyverse team
challenges you may be having with our packages. It collects data about
what commands you are typing interactively, any errors you encounter,
and can even help you submit github issues with any issues you may
encounter. You will have total control over submitting this information to the Tidyverse authors, and nothing will be sent without
an opportunity to review the information.

You have some options:

1. Have the option to send collaboratoR data for Tidyverse by default
2. NOT send collaboratoR data by default
3. Be asked again next time

Your choice: _
```

This default is probably pretty important to get right, so it's good 
to spend a few sentences on it.

#### How Defaults May Be Set

The setting of defaults could be set in a number of ways:

1. For users of a commercial IDE experience like RStudio's commercial offerings, the default could be set by a site administrator. It could be set up to not allow being override, or could potentially have per-user access to allow certain users (who work on open source in an enterprise) to collaborate
2. For users of open source IDEs, the default could be part of project intialization or templates. There could also be a default default inside of the IDE preferences.

### Continuing our Sample Session

In this scenario, the user will select `3. Be Asked again next time` 
since they're not particularly sure what the value proposition is. That's ok.

```
Your choice: 3

Thank you! We will ask you again next time. To learn more about the
collaboratoR package check out our github here http://blahblah.

To enable collaboratoR support for the `tidyverse` package for this
session, just type:

 collaboratoR::collaborate(tidyverse) at the interactive prompt when
 you are ready to start collaborating with the tidyverse authors! You
 can type ?collaboratoR::collaborate to learn all the features. 
 
 > print("Hello")
 [1] "Hello"
> collaboratoR::collaborate()
You are attempting to enable collaboratoR without a package. This will
enable for all packages that support. To enable collaboration for a 
single package, you can call it with a package name like collaboratoR::collaborate(ggplot2) and this will collaborate with just
the ggplot authors.

Did you want to enable collaboratoR for all packages (Y/N)? _
```

The user should be in control and understand exactly what is happening
at all times. In this section we see one of the interesting proposed
features of collaboratoR, that it would be a plug-in infrastructure
that all package authors could enable for their packages! Let's spend a
few sentences imagining how an author may enable `collaboratoR` support
for their package.

### How Package Authors Enable collaboratoR support

It should be straightforward for package authors to add support for
collaboratoR to their package. It would likely leverage the existing
infrastructure of the DESCRIPTION file. A description file can have
entries such as:

* Maintainer - The person who should be tagged in collaboratoR repports
* BugReports - The URL where collaboratoR will submit reports and data
* Suggests - Adding collaboratoR to the suggests list would be enough to enable collaboratoR support to a package

Given R's advanced meta-programming ability, this should be enough to
enable users to collaborate with them.

### Continuing our Sample Session

In our exmaple, our user will continue and enable collaboratoR support
for all packages for this session.

```
Did you want to enable collaboratoR for all packages (Y/N)? Y
Ask again about enabeling collaboratoR support for all packages by 
default in the future? Y

Thank you, we will ask again if you attempt to enable collaboratoR 
support without specifying a package in the future.

Congratulations! collaboratoR support is now enbled. We are logging
your session, at the end of your session you will have an option to
review your stats before sending anything to package authors.

> 
```

AT this point the user proceeds to do some interactive analysis. In a 
perfect world, one can imagine a few nice UI features that could be
developed over time. You can imagine

* An RStudio pane which shows you real time stats about this `collaboratoR` session as well as historical sessions.
* User hints as they use different package features if there have been collaboratoR reports for those features
* Etc...

So, let's imagine that the user does some stuff...

```
> library(data.table)
data.table 1.11.2
  The fastest way to learn (by data.table authors): https://www.datacamp.com/courses/data-analysis-the-data-table-way
  Documentation: ?data.table, example(data.table) and browseVignettes("data.table")
  Release notes, videos and slides: http://r-datatable.com

Attaching package: ‘data.table’

The following objects are masked from ‘package:dplyr’:

    between, first, last

The following object is masked from ‘package:purrr’:

    transpose
    
The following infrastructure has been enabled:

    collaboratoR

> # enter some analysis here
> library(h2o)
----------------------------------------------------------------------

Your next step is to start H2O:
    > h2o.init()

For H2O package documentation, ask for help:
    > ??h2o

After starting H2O, you can use the Web UI at http://localhost:54321
For more information visit http://docs.h2o.ai

----------------------------------------------------------------------


Attaching package: ‘h2o’

The following objects are masked from ‘package:data.table’:

    hour, month, week, year

The following objects are masked from ‘package:stats’:

    cor, sd, var

The following objects are masked from ‘package:base’:

    &&, %*%, %in%, ||, apply, as.factor, as.numeric, colnames,
    colnames<-, ifelse, is.character, is.factor, is.numeric, log, log10,
    log1p, log2, round, signif, trunc
    
The following infrastructure has been enabled:

    collaboratoR

> # some modeling happens here

```

At this point, the user has spent a bunch of time leveraging `tidyverse`, `data.table`, `h2o` and a bunch of other packages. In the
background, collaboratoR has been recording:

1. Every successful command and metadata
2. Every command that generated an error message
3. Etc...

The user has had a great session, and they are done doing analysis for 
a while. As they go to close the project or shut down RStudio, they 
would be asked if they wanted to submit their information. This could
be done via the RStudio UI if it available, or through the console. 
This is a rough idea of what it would look like:

```
> q()

collaboratoR Message
--------------------

Before you quit your RStudio session, let's review your potential 
contributions and review your options and your session!

* You have executed 112 sucessful commands in this 2 hour 31 minute session
* You had 23 error messages, you had:
 * 11 from collaboratoR supported packages:
 * 8 from tidyverse
 * 2 from h20
 * 1 from data.table

Would you like to:

1. Submit your collaboratoR findings
2. Save your collaboratoR findings in this project for later
3. Discard all collaboratoR data

Your choice: _

```

The user has the ability to submit directly right now or save for 
later. If the user chose to save off the collaboratoR findings, they
could be picked up in the next R session which leverages this project.

In this example, the user is going to submit their findings.

```
Your choice: 1

Awesome! Let me prepare your reports. Your session will generate 3
reports. You do not have any report defaults, so we have to ask you
a few questions about how much you would like to share. You will have
the ability to customize all of this in the final stages:

1. Provide all collaboratoR information, including parameters to functions. **Note**, this may leak information like specific parameters or file names. While this provides the most information to package authors make sure you understand these repercussions!
2. Provide scrubbed collaboratoR information. This sends information about function calls and errors, but scrubs any strings and numbers from the report, replacing them with placehodlers.
3. Provide just call and error information. This sends the minimum information to package authors, just telling them which commands you used and what errors were triggered.
4. Never mind, I don't want to do this now, take me back to the previous question.

Which would you like to provide: _

```

This is obviously a clumsy bad version of this idea, but what's the 
goal? To make it easy for the user to decide how much data they
want to share with the original package authors. The user should be
able to easily chose between showing everything, or providing just
the most minimal information. There is probably a more elegant way
to phrase this :)

```
Which would you like to provide: 1

Awesome, you will be creating collaboratoR reports which share all of
the data about your interations with the author's packages. 

`tidyverse` report
------------------

This is some high level information about your interaction with the
tidyverse package and the packages that it imports:

* You used 45 functions from packages imported by the tidyverse that
support collaboratoR. 
* You used the function `ggplot2::ggplot` the most successfuly, using it 22 times. 
* You had the most errors using `tidyr::spread`, you had 7 errors using this function. 

This report is going to be sent to Hadley Wickham <hadley@rstudio.com>.

Creating an issue with attachments in https://github.com/tidyverse/tidyverse/issues...

`data.table` report
-------------------

This is some high level information...

```

At this point, a browser would open up to a pre-filled GitHub issue for each major package. This issue would have the call and session data as attachments, as well as a textual narrative in the issue text.

The narrative would have obvious areas for the user to enter in information, stuff like:

* Most challenging function was tidyr::spread - [ENTER WHAT YOUR CHALLENGES WERE WITH THIS FUNCTION HERE. WHAT WERE YOU TRYING TO ACCOMPLISH AND WHAT COULD BE MADE BETTER]
* Function I used the most was ggplot2::gpplot - [ENTER WHAT YOU LVOE ABOUT THIS FUNCTION AND ANYTHING THAT MAY BE USEFUL FOR THE PACKAGE AUTHOR]

It would also have at the bottom every single call, with all the 
parameters in the following form:

```
Calls by Package, make sure this is all ok before final submission:

* ggplot2
** ggplot(mydataframe, aes(x=disease, y=age) 
** geom_point()
** geom_point(aes(color="red"))
** etc...
* forcats
** fct_lump(f=asdf, n=3)
** ...
```

This should be desgined to allow the user to make sure that everything was fine, and no data was exfiltrated which could be problematic. It would be important to make sure to figure out what the right way to structure these reports and their associated data as useful. 

## Conclusion

This is just a rough sketch of how such a thing could be experienced. It could give users the ability to provide feedback and incredibly valuable usage data easily and in an
automated fashion. It would give package authors this information and help them understand how users leverage their packages.

Please feel free to file isues an submit pull requests to make this idea better.